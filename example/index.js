const {Sequelize,  Model} = require('sequelize');
const sequelize = new Sequelize('sqlite::memory:')
const {transform, extractSchema} = require('../lib');

const accountModel = require('../tests/mocks/account');

const dummy = 
// const Account = transform(sequelize, accountModel, "account")

// Account.sync().then(() => {
//   Account.create(dummy)
// })


class Account extends Model {}
const schema = extractSchema(accountModel);
Account.init(schema, { sequelize, modelName: 'account' });

sequelize.sync()
  .then(() => Account.create(dummy))
  .then(jane => {
    console.log(jane.toJSON());
  });



// console.log(schema)
