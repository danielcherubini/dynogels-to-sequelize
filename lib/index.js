const { DataTypes } = require('sequelize');
/**
 * @param {array} schemaArray
 * @returns {object}
 */
function parseSchema(schemaArray) {
  let returnSchema = {};

  schemaArray.forEach((value) => {
    const parsedType = value._type ? value._type : value.schema._type;
    const type = DataTypes[parsedType.toUpperCase()];
    const flags = value._flags ? value._flags : value.schema._flags;
    const required = flags.presence === 'required';
    switch (parsedType.toUpperCase()) {
      case 'ARRAY':
        returnSchema[value.key] = [parseSchema(value.schema._inner.items)];
        break;
      case 'OBJECT':
        returnSchema = parseSchema(value._inner.children);
        break;
      default:
        returnSchema[value.key] = {
          type,
          required,
        };
        break;
    }
  });

  return returnSchema;
}

/**
 * @param {object} object
 * @returns {array}
 */
function extractSchemaKeys(object) {
  if (!object.schema._modelSchema) {
    throw new Error('could not find schema');
  }

  return object.schema._modelSchema._inner.children;
}

/**
 * @param {import('sequelize').Sequelize} sequelize
 * @param {object} dynogelsModel
 * @param {string} tableName
 * @returns {import('sequelize').Model}
 */
function transform(sequelize, dynogelsModel, tableName) {
  const extractedSchemaKeys = extractSchemaKeys(dynogelsModel);
  const schema = parseSchema(extractedSchemaKeys);

  return sequelize.define(tableName, schema);
}

/**
 * @param {object} dynogelsModel
 * @returns {object}
 */
function extractSchema(dynogelsModel) {
  const extractedSchemaKeys = extractSchemaKeys(dynogelsModel);
  return parseSchema(extractedSchemaKeys);
}

module.exports = {
  extractSchemaKeys,
  parseSchema,
  transform,
  extractSchema,
};
