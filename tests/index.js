const test = require('ava');
const { Sequelize, Model } = require('sequelize');
const { extractSchemaKeys, parseSchema, transform } = require('../lib');

const sequelize = new Sequelize('sqlite::memory:');

const accountModel = require('./mocks/account');
const gamescore = require('./mocks/gamescore');

test('Account Model', async (t) => {
  const Account = transform(sequelize, accountModel, 'account');

  await Account.sync();
  const result = await Account.create({
    name: 'janedoe',
    age: 32,
    email: 'foo@bar.com',
  });

  t.is(result.dataValues.name, 'janedoe');
  t.is(result.dataValues.age, 32);
  t.is(result.dataValues.email, 'foo@bar.com');
});


test('GameScore Model', (t) => {
  class GameScore extends Model {}
  const extractedSchemaKeys = extractSchemaKeys(gamescore);
  const schema = parseSchema(extractedSchemaKeys);

  const Foo = GameScore.init(schema, { sequelize, modelName: 'GameScore' });
  t.is(typeof Foo, 'function');
  const acc = new Foo();
  t.is(typeof acc._modelOptions, 'object');
});
