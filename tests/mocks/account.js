const dynogels = require('dynogels');
const Joi = require('joi');

// const AWS = dynogels.AWS;
// AWS.config.loadFromPath(`${process.env.HOME}/.ec2/credentials.json`);

dynogels.define('Account', {
  hashKey: 'name',
  rangeKey: 'email',
  schema: {
    name: Joi.string().required(),
    email: Joi.string(),
    age: Joi.number(),
    things: Joi.array().items(
      Joi.object().keys({
        foo: Joi.string()
      })
    )
  },
  indexes: [
    { hashKey: 'name', rangeKey: 'age', type: 'local', name: 'NameAgeIndex' },
  ]
});

module.exports = dynogels.models.Account;
