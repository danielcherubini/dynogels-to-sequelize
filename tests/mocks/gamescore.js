const dynogels = require('dynogels');
const Joi = require('joi');

dynogels.define('GameScore', {
  hashKey: 'userId',
  rangeKey: 'gameTitle',
  schema: {
    userId: Joi.string(),
    gameTitle: Joi.string(),
    topScore: Joi.number(),
    topScoreDateTime: Joi.date(),
    wins: Joi.number(),
    losses: Joi.number()
  },
  indexes: [{
    hashKey: 'gameTitle',
    rangeKey: 'topScore',
    type: 'global',
    name: 'GameTitleIndex',
    projection: { NonKeyAttributes: ['wins'], ProjectionType: 'INCLUDE' }
  }]
});

module.exports = dynogels.models.GameScore;
